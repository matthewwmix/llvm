# Requirements
```bash
# cmake v3.22 or > see cmake website.
wget https://github.com/Kitware/CMake/releases/download/v3.25.1/cmake-3.25.1-linux-x86_64.sh
chmod +x cmake-3.25.1-linux-x86_64.sh
./cmake-3.25.1-linux-x86_64.sh --skip-license --prefix=/usr/local

# Yum
yum install gcc gcc-c++ ninja-build -y
```

# Building with wasi
```bash
# Get the latest wasi libc
git clone https://github.com/WebAssembly/wasi-libc.git
cd wasi-libc
git checkout wasi-sdk-17
make -j 8

# Takes the newly created sysroot/ and places it under /usr/local/ for us
INSTALL_DIR=/usr/local/wasi-sysroot/ make install

# Get the runtime we need
wget https://github.com/WebAssembly/wasi-sdk/releases/download/wasi-sdk-17/libclang_rt.builtins-wasm32-wasi-17.0.tar.gz
# This generates lib/wasi/ directory with the runtime inside. We need to move that
tar -xzvf libclang_rt.builtins-wasm32-wasi-17.0.tar.gz
mv lib/ /usr/local/lib/clang/15.0.6/

clang --target=wasm32-wasi --sysroot=/usr/local/wasi-sysroot/ foo.c -o foo.wasm
```
