mkdir -p llvm-build-debug/
cmake -S llvm-project/llvm -B llvm-build-debug -G "Ninja" -DLLVM_INSTALL_UTILS=ON -DCMAKE_INSTALL_PREFIX="/usr/local" -DCMAKE_BUILD_TYPE="Debug" -DLLVM_TARGETS_TO_BUILD="host;X86;WebAssembly;NVPTX" -DLLVM_ENABLE_PROJECTS="clang;bolt;lld;lldb" -DLLVM_ENABLE_RUNTIMES="libcxx;libcxxabi" -DLLVM_USE_LINKER=lld
